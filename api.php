<?php
require __DIR__ . '/register.php';

if( isset($_POST["email"]) && isset($_POST["username"]) && isset($_POST["password"]) )
{
    $user = new User();
    try
    {
        $user->addNewUser($_POST["username"], $_POST["password"], $_POST["email"]);
        $response = [
            'success' => true
        ];
    }
    catch(Exception $e)
    {
      $response =  [
        'code' => $e->getCode(),
        'errorMessage' => $e->getMessage()
      ];
    }
    echo json_encode($response);
}