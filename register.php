<?php
class User 
{
  // Your server IP
  private $serverIp = "127.0.0.1,61433";

  // Your accounts table name
  private $user_table = "tbl_RFTestAccount";

  // Your database connection info
  private $loginInfo = [
      "Database" => "RF_User2",
      "UID" => "gamecp",
      "PWD" => "80QEHdupNA47yln"
  ];

  function addNewUser($userName, $password, $email)
  {
      try
      {
          $connection = sqlsrv_connect( $this->serverIp, $this->loginInfo);
          if (!$connection)
          {
            throw new Exception("Database connection error.");
          }
      }
      catch (Exception $e)
      {
          throw new Exception("Database connection error.");
      }
      
      // Make sure to update table fields to match yours
      $sql = "INSERT INTO " . $this->user_table . " (id,password,Email) 
              VALUES (
                  CONVERT (binary,?),
                  CONVERT (binary,?),
                  ?
              )";
      $params = array ($userName, $password, $email);
      
      try
      {
          $query = sqlsrv_query($connection, $sql, $params);
          if (!$query) 
          {
            throw new Exception("User creation error.");
          }
          sqlsrv_free_stmt($query);
          return true;
      } 
      catch (Exception $e)
      {
          throw new Exception("There was an SQL error while trying to add the account.");
      }
  }

  function checkUserName($userName)
  {
      // Make sure to update table fields to match yours
      $sql = "SELECT CONVERT(varchar, id) as id FROM " . $this->user_table . " WHERE id = CONVERT (binary,?)";
      $params = array ($userName);
      
      try
      {
          $result = sqlsrv_query(connection, $sql, $params);
          
          $row = sqlsrv_fetch_array($result);
          
          sqlsrv_free_stmt($query);
          
          return $row['id'];
      } 
      catch (Exception $e)
      {
          throw new Exception("ID check failure.");
      }
  }
}

